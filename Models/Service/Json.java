package Service;



import java.io.File;
import java.io.IOException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Json<T> implements IO<T> {

    private Class<T> clazz;

    public Json(Class<T> type) { this.clazz = type; }
    public Class<T> getClazz() {
        return clazz;
    }

    public void setClazz(Class<T> clazz) { this.clazz = clazz; }

    @Override
    public void toFile(T object, File file) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(file, object);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public T fromFile(File file) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(file, clazz);
        } catch (IOException e) {

            e.printStackTrace();
        }
        return null;
    }
}
