package Service;
import models.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DataBases {
    private String url=new String("jdbc:postgresql://localhost/javaData");
    private String user=new String("postgres");
    private String password=new String("yura2018");

    public void updateDish(int idDish,String name)
    {
        try {
            Connection db = DriverManager.getConnection(url, user, password);
            Statement statment = db.createStatement();
            ResultSet rest=statment.executeQuery("select max(dishid) from dishes;");
            rest.next();
            int id=rest.getInt("max");
            if(idDish<id &&idDish>0) {
                PreparedStatement ps = db.prepareStatement("update dishes set name= ? where dishid=?;");
                ps.setString(1,name);
                ps.setInt(2,idDish);
                ps.executeQuery();
            }
            else
                System.out.println("\n No correct id Dish\n");
        }
        catch (Exception e)
        {
         e.printStackTrace();
        }
    }
    public boolean deleteDish(int idDish){

        try {
            Connection db = DriverManager.getConnection(url, user, password);
            Statement statment = db.createStatement();
            ResultSet rest=statment.executeQuery("select max(dishid) from dishes;");
            rest.next();
            int id=rest.getInt("max");
            if(idDish<=id && idDish>0) {
                PreparedStatement ps = db.prepareStatement("delete from dishes where dishid=?;");
                ps.setInt(1,idDish);
                ps.execute();
                ps = db.prepareStatement("delete from recipe where recipeid=?;");
                ps.setInt(1,idDish);

                ps.execute();
                return true;
            }
            else
                return  false;
        }
        catch (Exception e)
        {
e.printStackTrace();
        }
        return true;
    }
    public  void insertDish(Dish dish){
        try {
            Connection db = DriverManager.getConnection(url, user, password);
            Statement statment = db.createStatement();
            ResultSet rest = statment.executeQuery("select max(dishid) from dishes;");
            rest.next();
            int idDish = rest.getInt("max");
            PreparedStatement ps = db.prepareStatement("insert into dishes(dishid,name,price) values(?,?,?);");
            ps.setInt(1,idDish+1);
            ps.setString(2,dish.getName());
            ps.setInt(3,dish.getPrice());
            ps.execute();
            rest=statment.executeQuery("select max(rawitemid) from rawitem;");
            rest.next();
             int id=rest.getInt("max")+1;
              int []idItem=new int[dish.getRecipe().size()];
              for(int i=0;i<dish.getRecipe().size();i++)
              {
                  System.out.println("ok");
                  ps=db.prepareStatement("insert into rawitem(rawitemid,name) values(?,?);");
                  ps.setInt(1,id+i);
                  String name=dish.getRecipe().get(i).getProduct().getName();
                  ps.setString(2,name);
                  idItem[i]=id+i;
                  ps.execute();
              }
              for(int i=0;i<idItem.length;i++)
              {
                  ps=db.prepareStatement("insert into recipe(recipeid,rawitemid,quant) values(?,?,?);");
                  ps.setInt(1,idDish+1);
                  ps.setInt(2,idItem[i]);
                  ps.setInt(3,dish.getRecipe().get(i).getCount());
                  ps.execute();
             }
        }
        catch (Exception e)
        {

        }
    }
    public void writeDataBasesDish(Collection<Dish> Dishes)
    {

        try {
            Connection db = DriverManager.getConnection(url, user, password);
            Statement statment= db.createStatement();
            ResultSet rest=statment.executeQuery("select dishid,name,price from dishes;");
            while (rest.next())
            {
                List<Recipe> recipes=new ArrayList<>();
                String name =new String(rest.getString("name"));
                int id =rest.getInt("dishid");
                int price=rest.getInt("price");
                PreparedStatement ps=db.prepareStatement("select distinct rawitem.name,recipe.quant from dishes, rawitem,recipe where recipe.recipeid=? and  rawitem.rawitemid=recipe.rawitemid;" );
                ps.setInt(1,id);
                ResultSet rs=ps.executeQuery();
                while (rs.next())
                {
                    int quant=rs.getInt("quant");
                    String raw=rs.getString("name");
                    RawItem rawItem=new RawItem.Builder().setName(raw).build();
                   Recipe recipe=new Recipe.Builder().setProduct(rawItem).setCount(quant).build();
                    recipes.add(recipe);
                }
                Dish dish=new Dish.Builder().setName(name).setPrice(price).setRecipe(recipes).build();
                Dishes.add(dish);
            }

        } catch (SQLException e) {
            e.printStackTrace();

        }

    }
    public int OrderPrice(int idOrder)
    {
        int price=0;

        try {
            Connection db = DriverManager.getConnection(url, user, password);
            Statement statment= db.createStatement();
            PreparedStatement ps=db.prepareStatement("select sum(dishes.price) from dishes,orders,ordersitem where orders.orderid=? and orders.orderid=ordersitem.orderid and  ordersitem.dishid=dishes.dishid;");
            ps.setInt(1,idOrder);
            ResultSet rs=ps.executeQuery();
            rs.next();
            price=rs.getInt("sum");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return price;
    }
    public Dish dearDish(int idOrder)
    {
        try {
            Connection db = DriverManager.getConnection(url, user, password);
            Statement statment = db.createStatement();
            PreparedStatement ps=db.prepareStatement("select dishes.name,dishes.price from dishes,orders,ordersitem where orders.orderid=? and dishes.dishid=ordersitem.dishid and orders.orderid=ordersitem.orderid order by dishes.price desc;");
            ps.setInt(1,idOrder);
            ResultSet rs=ps.executeQuery();
            rs.next();
            String name=rs.getString("name");
            int price=rs.getInt("price");
            ps=db.prepareStatement("select distinct rawitem.name,recipe.quant from dishes, rawitem,recipe where dishes.name=? and recipe.recipeid=dishes.dishid and  rawitem.rawitemid=recipe.rawitemid;" );
            ps.setString(1,name);
            rs=ps.executeQuery();
            List<Recipe> recipes=new ArrayList<>();
            while (rs.next())
            {
                int quant=rs.getInt("quant");
                String raw=rs.getString("name");
                RawItem rawItem=new RawItem.Builder().setName(raw).build();
                Recipe recipe=new Recipe.Builder().setProduct(rawItem).setCount(quant).build();
                recipes.add(recipe);
            }
            Dish dish=new Dish.Builder().setName(name).setPrice(price).setRecipe(recipes).build();
            return  dish;
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
    public void writeProvider(Collection<Provider> providers)
    {
        try {
            Connection db = DriverManager.getConnection(url, user, password);
            Statement statment = db.createStatement();
            ResultSet rest=statment.executeQuery("select name,contact from providers;");
            while (rest.next())
            {
                Provider provider=new Provider.Builder().setName(rest.getString("name")).setContact(rest.getInt("contact")).build();
                providers.add(provider);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
       public  void writeOrdes(Collection<Order>orders)
       {
           Collection<Dish> dishes=new ArrayList<>();
           writeDataBasesDish(dishes);
           Collection<Provider> providers=new ArrayList<>();
           writeProvider(providers);
           try {
               Connection db = DriverManager.getConnection(url, user, password);
               Statement statment = db.createStatement();
               ResultSet rest=statment.executeQuery("select orderid,providerid from orders;");
               while(rest.next()) {
                   int idOrder = rest.getInt("orderid"), idProvider = rest.getInt("providerid");
                    Provider provider=((ArrayList<Provider>) providers).get(idProvider-1);
                    PreparedStatement ps=db.prepareStatement("select orders.orderid,orders.providerid,ordersitem.dishid from orders,ordersitem where orders.orderid=ordersitem.orderid and orders.orderid=?;");
                   ps.setInt(1,idOrder);
                   ResultSet rs=ps.executeQuery();
                   List<Dish> list=new ArrayList<>();
                   while (rs.next()) {
                       int idDish=rs.getInt("dishid");
                       list.add(((ArrayList<Dish>) dishes).get(idDish-1));
                    }
                    Order order=new Order();
                   order.setProvider(provider);
                   order.setDish(list);
                   orders.add(order);
               }
           }
           catch (Exception e){
               e.printStackTrace();
           }
       }
}
