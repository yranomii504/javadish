package Service;

import java.io.File;
import models.*;

public interface IO<T> {

    void toFile(T object, File file);

    T fromFile(File file);

    Class<T> getClazz();

    void setClazz(Class<T> clazz);

}
