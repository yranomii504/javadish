package Service;

import models.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Service {
    /**
     * max price dish in order
     * return Dish
     */
    public static Dish maxPriceDish(Order order)
    {
        Dish obDish=null;
        List<Dish> listDish;
        listDish=order.getDish();
        int maxPrice=0;

        for(Dish ob:listDish)
        {
            if(maxPrice<ob.getPrice())
           {
               obDish=ob;
               maxPrice=ob.getPrice();
           }
        }
        return obDish;
    }

    /**
     *
     * @param dish
     * @return ListRecipeItem
     */
    public static List<Recipe> recipesDish(Dish dish)
{
    return dish.getRecipe();
}

    /**
     *
     * @param order
     * @return provider
     */
    public static Provider informationProvider(Order order)
    {
    return order.getProvider();
    }
    public static Stream<Dish> filterDishes(List<Dish> dishes, int filter){
       Stream<Dish> list=dishes.stream().filter(dish -> dish.getPrice()>filter);
       return list ;
    }
    /**
     *
     * @param order
     * @return priceOrder
     */
    public static int priceOrder(Order order)
{
    int priceOrder=0;
    List<Dish> dishes=order.getDish();
    for(Dish ob:dishes)
        priceOrder+=ob.getPrice();

    return  priceOrder;
}
  public String RecipeDish(Dish dish){
       String recipe=new String(dish.toString());
       return recipe;
  }
}
