package models;


import org.jetbrains.annotations.NotNull;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.regex.Matcher;
@XmlRootElement
@XmlType(propOrder = {"name","contact"})
public class Provider implements Serializable {
    @NotNull
    public int providerId;
    @NotNull

    public  String name;
    @NotNull

    public  int contact;

    public Provider() {

    }
    private Provider(Builder builder)
    {
        this.name=builder.name;
        this.contact=builder.contact;
    }

@Override
    public boolean equals(Object obj) {
        if(this==obj) return true;
        if(obj==null|| getClass()!=obj.getClass())return false;
        Provider provider=(Provider)obj;
        return name==provider.name&&contact==provider.contact;
    }
    @Override
    public int hashCode()
    {
        int k = this.name.length();
        int u = 0,n = 0;
        byte []el=name.getBytes();
        for (int i=0; i<k; i++)
        {
            n = el[i];
            u += 6*n%31;
        }
        return u%138;
    }
    public  void setName(String name){ this.name=name;}
    public void setContact(int contact){this.contact=contact;}
    public String getName()
    {
        return name;
    }

    public int getContact()
    {
        return contact;
    }
@Override
   public String toString()
    {
        String str=new String("Provider>>"+name+"phone>>"+contact);
        return  str;
    }
 public class Builder{
     public String  name;
     public int contact;
     private Builder(){}
        public Builder setName(String name)
        {
            this.name=null;
            java.util.regex.Pattern p= java.util.regex.Pattern.compile("[A-Za-z-']{1,30}");
            Matcher m=p.matcher(name);
            try {
                if(!m.matches()) throw new Exception("Name=Null");
                this.name=name;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return this;
        }
        public Builder setContact(int contact){
            this.contact=0;
            try
            {
                 if(contact<10000) throw new Exception("Not correct contact");
                 this.contact=contact;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return this;
        }
        public Provider build()
        {
            return new Provider(this);
        }

 }
}
