package models;

import org.jetbrains.annotations.NotNull;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;
@XmlRootElement
@XmlType(propOrder = {"provider","dish"})
public class Order implements Serializable {
    @NotNull
    private int orderId;
    @Valid
    private  Provider provider;
    @Valid
    private  List<Dish> dish;
    public Order()
    {
    }
    public void setProvider(Provider provider)
    {
        this.provider=provider;

    }
    public void setDish(List<Dish> dish)
    {
        this.dish=dish;
    }
    @Override
    public int hashCode()
    {
        int key=this.provider.getContact();
        key = (~key) + (key << 21); // key = (key << 21) - key - 1;
        key = key ^ (key >>> 24);
        key = (key + (key << 3)) + (key << 8); // key * 265
        key = key ^ (key >>> 14);
        key = (key + (key << 2)) + (key << 4); // key * 21
        key = key ^ (key >>> 28);
        key = key + (key << 31);
        return key;
    }
    @Override
    public boolean equals(Object obj) {
        if(this==obj) return true;
        if(obj==null|| getClass()!=obj.getClass())return false;
        Order order=(Order)obj;
        return provider==order.provider&&dish==order.dish;
    }
    public Provider getProvider()
    {
        return provider;
    }
    public  List<Dish> getDish()
    {
        return  dish;
    }
     @Override
    public String toString()
    {
        String str=new String("Order: "+provider.getName());
        for(int i=0;i<dish.size();i++)
           str=str+"name dish: "+dish.get(i).getName();

        return str;
    }


}
