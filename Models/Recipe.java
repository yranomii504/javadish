package models;

import org.jetbrains.annotations.NotNull;

import javax.validation.Valid;
import java.io.Serializable;
public class Recipe implements Serializable {
    @NotNull
    private  int recipeId;
    @Valid
    private RawItem product;
    @NotNull
    private int count;

    public Recipe() { }
    private Recipe(Builder builder){
        this.product=builder.product;
        this.count=builder.count;
    }
    @Override
    public int hashCode()
    {
        int k = product.getName().length();
        int u = 0,n = 0;
        byte []el=product.getName().getBytes();
        for (int i=0; i<k; i++)
        {
            n = el[i];
            u += 8*n%31;
        }
        return u%140;

    }
@Override
    public boolean equals(Object obj) {
        if(this==obj) return true;
        if(obj==null|| getClass()!=obj.getClass())return false;
        Recipe recipe=(Recipe)obj;
        return product==recipe.product&&count==recipe.count;
    }

    public int getCount()
    {
        return count;
    }
public  void setCount(int count){this.count=count;}
    public RawItem getProduct()
    {
        return product;
    }
public void setProduct(RawItem product){this.product=product;}
@Override
    public String toString()
    {
        String str=new String(" name product: "+product.getName()+", count product: "+count);
        return str;
    }
    public static class Builder{
        private  RawItem product;
        private  int count;
        public Builder()
        {

        }
       public Builder setCount(int Count)
       {
           this.count=0;
           try {
               if(count<=0&&count>20) throw new Exception("Not correct count raw");
               this.count=Count;
           }
           catch (Exception e)
           {
               e.printStackTrace();
           }

           return this;
       }
       public Builder setProduct(RawItem product)
       {
           this.product=product;
           return this;
       }
        public Recipe build()
        {
            return new Recipe(this);
        }
    }

}
