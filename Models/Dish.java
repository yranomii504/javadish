package models;
import org.jetbrains.annotations.NotNull;
import  javax.validation.Valid;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.Size;

import java.util.ArrayList;
import java.util.List;
import java.io.*;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@XmlRootElement
@XmlType(propOrder = {"name", "price", "recipe"})
public class Dish implements Serializable{
    @NotNull
    private int dishId;
    @Size(max=45,message = "Max size name 45")
    private  String name;
    @NotNull
    private  int price;
    @Valid
    private  List<Recipe> recipe=new ArrayList<>();

  public Dish()
  {

  }
    private Dish(Builder builder){
        this.name = builder.name;
        this.price = builder.price;
        this.recipe = builder.recipe;
    }

    @Override
    public boolean equals(Object obj) {
        if(this==obj) return true;
        if(obj==null|| getClass()!=obj.getClass())return false;
        Dish dish=(Dish)obj;
        return name==dish.name&&price==dish.price;
    }
    @Override
    public int hashCode()
    {
         int k = this.name.length();
         int u = 0,n = 0;
         byte []el=name.getBytes();

        for (int i=0; i<k; i++)
        {
            n = el[i];
            u += 7*n%31;
        }
        return u%139;
    }
public  void setName(String name)
{
    this.name=name;
}
    public String getName()
    {
        return name;
    }
   public  void setPrice(int price){
      this.price=price;
   }
    @JsonIgnore
    public void setRecipe(List<Recipe> recipe)
    {
        this.recipe=recipe;
    }
    public int getPrice()
    {
        return price;
    }
    @JsonProperty
    public List<Recipe> getRecipe()
    {
        return recipe;
    }
    @Override
    public String toString()
    {
        int i;
        String str=new String("Dish>>"+"name: "+name+",price: "+price+", recipe: ");
         for(i=0;i<recipe.size();i++)
             str=str+recipe.get(i).toString();

        return str;
    }
    public static class Builder {
        private String name;
        private int price;
        private List<Recipe> recipe ;

        public  Builder setName(String name) {


            this.name = null;
            Pattern p=Pattern.compile("[A-Za-z-']{1,30}");
            Matcher m=p.matcher(name);
            try {
                if (!m.matches()) throw new Exception("Name=null");
                this.name = name;
            } catch (Exception e) {
                e.printStackTrace();
            }


            return this;
        }

        public Builder setPrice(int price) {
            this.price = 0;
            try {
                if (price < 0 && price > 500) throw new Exception("Price not in 0-500$");
                this.price = price;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return this;
        }

        public Builder setRecipe(List<Recipe> recipe) {
            this.recipe = null;
            try {
                if (recipe.isEmpty())
                    throw new Exception("Recipe=Null");
                this.recipe = recipe;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return this;

        }
        public Dish build(){
            return new Dish(this);
        }

    }
      public void writefile(String file)
      {
          try {
              FileWriter wrt=new FileWriter(file);
              wrt.write(this.toString());
              wrt.close();
          } catch (IOException e) {
              e.printStackTrace();
          }
      }
      public void readfile(String file){


          List<Recipe> recipes = new ArrayList<>();
          FileReader read = null;
          try {
              read = new FileReader(file);
          } catch (FileNotFoundException e) {
              e.printStackTrace();
          }
          Scanner scan = new Scanner(read);
          String name = scan.nextLine();
          int price = Integer.parseInt(scan.nextLine());
          while (scan.hasNextLine()) {
             RawItem product = new RawItem.Builder().setName(scan.nextLine()).build();
              Recipe recipe = new Recipe.Builder().setProduct(product).setCount(Integer.parseInt(scan.nextLine())).build();
              recipes.add(recipe);}
          Dish dish = new Dish.Builder().setName(name).setPrice(price).setRecipe(recipe).build();
           this.name=dish.name;
           this.recipe=dish.recipe;
           this.price=dish.price;
      }


}
