package models;
import org.jetbrains.annotations.NotNull;
import java.io.Serializable;
import java.lang.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class RawItem implements Serializable {
    @NotNull
    private int rawId;
    @NotNull
     private String name;

   public RawItem()
   {

   }
   private RawItem(Builder builder){
       this.name=builder.name;
   }
    @Override
    public int hashCode()
    {
        int k = this.name.length();
        int u = 0,n = 0;
        byte []el=name.getBytes();
        for (int i=0; i<k; i++)
        {
            n = el[i];
            u += 8*n%31;
        }
        return u%140;
    }
    @Override
    public boolean equals(Object obj) {
        if(this==obj) return true;
        if(obj==null|| getClass()!=obj.getClass())return false;
        RawItem raw=(RawItem) obj;
        return name==raw.name;
    }

 public void setName(String name){
       this.name=name;
 }
    public String getName()
    {
        return name;
    }

 @Override
     public String toString()
 {
    return  name;
 }
  public static class Builder{
       public String name;
       public Builder()
       {
       }
       public Builder setName(String name)
       {
           this.name=null;
           java.util.regex.Pattern p= Pattern.compile("[A-Za-z-']{1,40}");
           Matcher m=p.matcher(name);
           try {
               if (!m.matches()) throw new Exception("Name=null");
             this.name=name;
           }catch (Exception e)
           {
               e.printStackTrace();
           }
           return this;

       }
       public RawItem build()
       {
           return new RawItem(this);
       }

  }
}
