package main;
import Interface.*;
import models.*;
import Service.*;
import  java.io.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        final Random random=new Random();
        RawItem []rawItems=new  RawItem[10];
        List<Recipe> recipes=new ArrayList<>();
        Recipe recipeItem;
        Collection<Dish> dishes=new ArrayList<>();
        Collection<Provider> providers=new ArrayList<>();
        Collection<Order> orders=new ArrayList<>();
        // Databases

        DataBases data=new DataBases();
       /* data.writeDataBasesDish(dishes);
        data.writeProvider(providers);
        data.writeOrdes(orders);
       for(int i=0;i<orders.size();i++){
           System.out.println(((ArrayList<Order>) orders).get(i).toString());
        }
        Stream<Dish> dishList= Service.filterDishes(((ArrayList<Order>) orders).get(14).getDish(),170);
        dishList.forEach(ob->System.out.println(ob.toString()));
        System.out.println(Service.maxPriceDish(((ArrayList<Order>) orders).get(0)));
        for(int i=0;i<providers.size();i++)
        System.out.println(((ArrayList<Provider>) providers).get(i).toString());
       for (int i=0;i<dishes.size();i++)
        {
            System.out.println(((ArrayList<Dish>) dishes).get(i).toString());
        }
        Dish dish=data.dearDish(2);
        System.out.println("\nDear"+dish.toString());
        int price=data.OrderPrice(12);
        System.out.println("\nOrder price>>"+price);
        */
        //Xml
        /*
        File  file=null;
        file=new File("dish.xml");
        IO io=null;
        Class clazz=null;
        clazz=Dish.class;
        io=new Xml(Dish.class);
        io.setClazz(clazz);
        io.toFile(dish,file);
        Dish test=new Dish();
        test=(Dish) io.fromFile(file);
        System.out.println(test.toString());
        */

        // Json
        /*
        File to;
        to=new File("Json.json");
        Class clazz=null;
        clazz=Dish.class;
        IO io=null;
        io=new Json(Dish.class);
        io.setClazz(clazz);
        io.toFile(dish,to);
        Dish test=new Dish();
        test=(Dish)io.fromFile(to);
        System.out.println(test);
        */

       //boolean flag= data.deleteDish(1);
       // data.updateDish(7,"Oland");

        System.out.println("Enter name 4 product>>");
        Scanner in = new Scanner(System.in);
        for (int i=0;i<4;i++)
        {
            rawItems[i]=new RawItem.Builder().setName(in.nextLine()).build();
            recipeItem=new Recipe.Builder().setProduct(rawItems[i]).setCount(random.nextInt(20)+1).build();
            recipes.add(recipeItem);
        }
        data.deleteDish(29);
        Dish dish1=new Dish.Builder().setName("chek").setPrice(20).setRecipe(recipes).build();
        Dish dish2=new Dish.Builder().setName("dist").setPrice(20).setRecipe(recipes).build();
        List<Dish> list=new ArrayList<>();

        //data.insertDish(dish1);
        //dish.readfile("Dishread.txt");
        //dish.writefile("dishFile");
        //list.add(dish);
        list.add(dish1);
        list.add(dish2);
        Provider provider=new Provider.Builder().setName("Vasua").setContact(125168).build();
        Order order=new Order();
        order.setDish(list);
        order.setProvider(provider);
        int price= Service.priceOrder(order);
        System.out.println("Price>>"+price);
        return;
    }
}
